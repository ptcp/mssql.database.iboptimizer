﻿CREATE TABLE [dbo].[published_banks_sector_details] (
    [lpfPeriod]    DATE         NULL,
    [userName]     VARCHAR (50) NULL,
    [submissionID] VARCHAR (50) NULL,
    [sectorID]     FLOAT (53)   NULL,
    [sectorName]   VARCHAR (50) NULL,
    [weightManual] FLOAT (53)   NULL,
    [limitSector]  FLOAT (53)   NULL
);

